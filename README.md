# Prometheus operator Helm chart for Kubernetes (advanced way)

## Intro

There is a lot of ways to integrate Prometheus into your system.
How it considered in an [official document](https://prometheus.io/docs/prometheus/latest/installation/) we can have used the configuration management systems.

If you prefer using configuration management systems you might be interested in the following third-party contributions:

- Ansible  [Cloud Alchemy/ansible-prometheus](https://github.com/cloudalchemy/ansible-prometheus)

- Chef [rayrod2030/chef-prometheus](https://github.com/rayrod2030/chef-prometheus)

- Puppet [puppet/prometheus](https://forge.puppet.com/puppet/prometheus)

- SaltStack [saltstack-formulas/prometheus-formula](https://github.com/saltstack-formulas/prometheus-formula)

To be fair, here it should mention GitLab Auto DevOp and GitLab Kubernetes Agent

- GilLab Autodevops [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/topics/autodevops/index.html)

"GilLab CI" and "GilLab Autodevops" tools which auto-create pipelines ci/cd and, configures Prometheus integrations in many of these areas also including security auditing and vulnerability testing. It can be helpful to reduce the complexity of integration and configuration.

Your gitlab projet page > Project sidebar > Settings > Integration > Add an integration list > Prometheus 
or add the "/-/services/prometheus/edit" into to tail URL your gitlab projet page.

## Project philosophy

But this topic presents a way to get an integrated and pre-configured infrastructure solution for your Kubernetes cluster using the Helm chart.

- Use the Helm chart.

**Special thanks to Nana Janashia**! In this [youtube-video](https://www.youtube.com/watch?v=mLPg49b33sA&t=1s). Nana takes a step-by-step look at the integration process and explains how to read, make the configuration and will give us a good overview of all the different options and a clear image of steps required to set up the monitoring.

<img src="imgs/server health concerns_observabillity.jpg">

## Prerequisites

- Kubernetes installed
- Helm V.3 installed

## Project structure


## 1. First we will deploy a Prometheus-operator chart to our Kubernetes cluster using a helm chart 

I don't know still, the shortest way, how to get the whole installed Prometheus, with already pre-configured infrastructure.
This is a pretty easy step.


Usage:
  helm repo add [NAME] [URL] [flags]

I prefer to use the stable instead Prometheus-community chart
Prometheus Operator shown in Nana's video is Deprecated.
and execution "helm install" might return appeared "failed to download" see [isue](https://gitlab.com/8ops/prometheus-operator-helm-chart-for-kubernetes/-/issues/1)


```
helm repo add stable https://charts.helm.sh/stable

helm repo update
```

I suppose we can use  also as alternative
Googlea-stable repository

```
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
```

But please also take a look on [prometheus-community helm-charts](https://prometheus-community.github.io/helm-charts)

See the git repo for new operator installation and other relevant commands: [kube-prometheus-stack](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack)

## 2. Install prometheus

Install prometheus as stateful application with k8s Operator.

```
helm install prometheus stable/prometheus-operator
```
if everything installed well
it will return:

"[repository-name]" has been added to your repositories

## 3. Creating configuration manifests

Finally, after installation we have a lot of components, see the kube-prometheus [README](https://github.com/prometheus-operator/kube-prometheus) for details about components, dashboards, and alerts described.

Configuration manifests to allow us change and deploy different parts Kubernetes cluster.

#prometheusmonitoring​ #kubernetes​ #devops